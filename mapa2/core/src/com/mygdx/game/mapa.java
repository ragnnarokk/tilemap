package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;

public class mapa extends ApplicationAdapter implements InputProcessor {

	Texture img;

	TiledMap tiledMap;

	OrthographicCamera camera;

	TiledMapRenderer tiledMapRenderer;

	SpriteBatch batch;

	int x = 0;
	int y = 0;

	ParticleEffect pe;

	@Override

	public void create() {

		batch = new SpriteBatch();

		pe = new ParticleEffect();

		pe.load(Gdx.files.internal("foc.part"),Gdx.files.internal(""));

		x = Gdx.graphics.getWidth()/2;
		y = Gdx.graphics.getHeight()/2;

		pe.setPosition(x,y);

		float w = Gdx.graphics.getWidth();

		float h = Gdx.graphics.getHeight();

		camera = new OrthographicCamera();

		camera.setToOrtho(false, w, h);

		camera.update();

		img = new Texture("tileset.png");

		tiledMap = new TmxMapLoader().load("Mapa.tmx");

		tiledMapRenderer = new OrthogonalTiledMapRenderer(tiledMap);

		pe.start();

		Gdx.input.setInputProcessor(this);

	}

	@Override

	public void render() {

		Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);

		Gdx.gl.glClearColor(0, 0, 0, 1);

		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		camera.update();

		tiledMapRenderer.setView(camera);

		tiledMapRenderer.render();

		pe.update(Gdx.graphics.getDeltaTime());

		pe.setPosition(x,y);

		batch.begin();

		pe.draw(batch);

		batch.end();

		if (pe.isComplete()) pe.reset();

	}

	@Override

	public boolean keyDown(int keycode) {

		return false;

	}

	@Override

	public boolean keyUp(int keycode) {

		if (keycode == Input.Keys.LEFT)

			camera.translate(-32, 0);

		if (keycode == Input.Keys.RIGHT)

			camera.translate(32, 0);

		if (keycode == Input.Keys.UP)

			camera.translate(0, -32);

		if (keycode == Input.Keys.DOWN)

			camera.translate(0, 32);

		if (keycode == Input.Keys.NUM_1) {
			pe.scaleEffect(0.8f);
			tiledMap.getLayers().get(0).setVisible(

					!tiledMap.getLayers().get(0).isVisible());
		}

		if (keycode == Input.Keys.NUM_2) {
			tiledMap.getLayers().get(1).setVisible(

					!tiledMap.getLayers().get(1).isVisible());
		}

		if (keycode == Input.Keys.NUM_3) {
			tiledMap.getLayers().get(2).setVisible(

					!tiledMap.getLayers().get(2).isVisible());
		}

		if (keycode == Input.Keys.NUM_4)
			pe.scaleEffect(1.2f);
			tiledMap.getLayers().get(3).setVisible(

					!tiledMap.getLayers().get(3).isVisible());

		if (keycode == Input.Keys.NUM_5)

			tiledMap.getLayers().get(4).setVisible(

					!tiledMap.getLayers().get(4).isVisible());

		if (keycode == Input.Keys.NUM_6)

			tiledMap.getLayers().get(5).setVisible(

					!tiledMap.getLayers().get(5).isVisible());

		if (keycode == Input.Keys.NUM_7)

			tiledMap.getLayers().get(6).setVisible(

					!tiledMap.getLayers().get(6).isVisible());

		if (keycode == Input.Keys.NUM_8)

			tiledMap.getLayers().get(7).setVisible(

					!tiledMap.getLayers().get(7).isVisible());

		return false;

	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		x = screenX;
		y = -screenY + Gdx.graphics.getHeight();
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}
}
